terraform {
  required_version = ">= 1.8.1" // terraform version below 0.12 is not tested/supported with this module
  required_providers {
    oci = {
      source  = "hashicorp/oci"      
      version = ">=5.36.0" 
    }  
    ocidouble = {
      source  = "oracle/oci"      
      version = ">=5.36.0" 
    }  
  }
}

provider "oci" {
  tenancy_ocid      = var.tenancy_ocid
  user_ocid         = var.user_ocid
  private_key_path  = var.privkey_path
  fingerprint       = var.fingerprint
  region            = var.region["vinhedo"]
}

provider "ocidouble" {
  tenancy_ocid      = var.tenancy_ocid
  user_ocid         = var.user_ocid
  private_key_path  = var.privkey_path
  fingerprint       = var.fingerprint
  region            = var.region["vinhedo"]
}

