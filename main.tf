#Doc sobre os recursos disponíveis para o free tier:
#https://docs.oracle.com/en-us/iaas/Content/FreeTier/freetier_topic-Always_Free_Resources.htm

### Cria compartment ###
resource "oci_identity_compartment" "compartment" {
  # Required
  compartment_id  = var.tenancy_ocid
  description     = var.compartment_desc
  name            = var.compartment_name
}

locals {
  compartment_id    = resource.oci_identity_compartment.compartment.id
  compartment_name  = resource.oci_identity_compartment.compartment.name
}


module "ovcn" {
  source = "./modules/vcn"
  compartment_id = local.compartment_id

  ip_cidr_blocks = [ var.ip_cidr_blocks ]
  vcn_name       = var.vcn_name
}


#### Internet Gateway ###
#module "igateway" {
#  source = "./modules/internet_gateway"
#
#  compartment_id  = local.compartment_id
#  vcn_id          = module.ovcn.id
#  route_table_id  = module.ovcn.default_route_table_id
#  ig_display_name = "terrario_ig"
#}


### Cria uma rede publica ###
module "public_subnet" {
  source          = "./modules/subnet"
  compartment_id  = local.compartment_id
  vcn_id          = module.ovcn.id

  cidr_block      = var.public_cidr_block
  dns_label       = var.pubnet_dns_label
  route_table_id  = module.ovcn.default_route_table_id

  ### PUBLIC NETWORK. RECEBE CONEXOES DE INTERNET ###
  block_internet_ingress = false
}



### Cria uma rede privada ###
module "priv_subnet" {
  source          = "./modules/subnet"
  compartment_id  = local.compartment_id
  vcn_id          = module.ovcn.id

  cidr_block      = var.priv_cidr_block
  dns_label       = var.privnet_dns_label
  route_table_id  = module.ovcn.default_route_table_id
 
  ### PRIVATE NETWORK. RECEBE CONEXOES DE INTERNET ###
  block_internet_ingress = true
}


### CPE - Customer-Premises Equipment ###
module "router" {
  source          = "./modules/cpe"
  compartment_id  = local.compartment_id
  cpe_name        = var.cpe_name
  cpe_ip          = var.cpe_ip
}

### Routes ###
#module "routes" {
#  source          = "./modules/routes"
#
#  compartment_id  = local.compartment_id
#  vcn_id          = module.ovcn_id
#  net_elem_id     = module.igateway.id
#  route_table_id  = module.ovcn.default_route_table_id
#  cidr_block      = var.public_cidr_block
#}


#### cria compute instance(s) ###
locals {
  priv_subnet_id  = module.priv_subnet.id
  pub_subnet_id   = module.public_subnet.id

  states      = [ "RUNNING", "STOPPED", "STOPPED", "STOPPED" ]
  use_pub_ip  = [true, false, false, false]
  subnet_ids  = [local.pub_subnet_id, local.priv_subnet_id, local.priv_subnet_id, local.priv_subnet_id]
  images      = [ "vinhedo", "vinhedo", "vinhedo", "vinhedo"]
}

module "hosts" {
  source          = "./modules/vm"
  compartment_id  = local.compartment_id

  instance_count  = var.instance_count
  shape           = var.shape
  ssh_pubkey_file = var.ssh_pubkey_file

  states      = local.states
  open        = local.use_pub_ip
  images      = local.images
  subnet_ids  = local.subnet_ids
}

