#### INTERNET GATEWAY ###
#resource "oci_core_internet_gateway" "world_gw" {
#    #Required
#    compartment_id = local.compartment_id
#    vcn_id = resource.oci_core_vcn.vcn.id
#
#    #Optional
#    enabled         = var.internet_gateway_enabled
#    display_name    = var.internet_gateway_display_name
#    freeform_tags   = { "terraformed": "Please do not edit manually" }
#    route_table_id  = resource.oci_core_vcn.vcn.default_route_table_id
#}
#
#
#### DYNAMIC ROUTING GATEWAY ###
#resource "oci_core_drg" "terrario_drg" {
#    compartment_id = local.compartment_id
#
#    #Optional
#    display_name = var.drg_display_name
#    freeform_tags = { "terraformed": "Please do not edit manually" }
#}
#
#resource "oci_core_drg_attachment" "op_drg_attachment" {
#    #Required
#    drg_id = oci_core_drg.terrario_drg.id
#
#    #Optional
#    display_name = "attachone"
#    freeform_tags = { "terraformed": "Please do not edit manually" }
#    network_details {
#        #Required
#        id = oci_core_vcn.vcn.id
#        type = "VCN"
#
#        #Optional
#        #route_table_id = oci_core_route_table.test_route_table.id
#        #vcn_route_type = var.drg_attachment_network_details_vcn_route_type
#    }
#}
#
#
#
#### ROUTE TABLES ###
#resource "oci_core_default_route_table" "default_vcn_route_table" {
#  manage_default_resource_id = oci_core_vcn.vcn.default_route_table_id
#  route_rules {
#      #Required
#      network_entity_id = oci_core_drg.terrario_drg.id
#      #Optional
#      description = "Rota IPv4 para on-premises"
#      destination = var.on_prem_cidr_block
#      destination_type = "CIDR_BLOCK"
#    }
#  route_rules {
#    #Required
#    network_entity_id = oci_core_internet_gateway.world_gw.id
#    description = "Rota IPv4 para o mundo"
#    destination = "0.0.0.0/0"
#    destination_type = "CIDR_BLOCK"
#  }
#}
#
#
#resource "oci_core_route_table" "drg_route_table" {
#  #Required
#  compartment_id = local.compartment_id
#  vcn_id = oci_core_vcn.vcn.id
#  #Optional
#  display_name = "Tabela de rotas de rede pública"
#  freeform_tags = { "terraformed": "Please do not edit manually" }
#  route_rules {
#      #Required
#      network_entity_id = oci_core_drg.terrario_drg.id
#      #Optional        
#      description = "Rota IPv4 para on-premises"
#      destination = var.on_prem_cidr_block
#      destination_type = "CIDR_BLOCK"
#    }
#    #route_rules {
#    #  #Required
#    #  network_entity_id = oci_core_drg.terrario_drg.id
#    #  #Optional        
#    #  description = "Rota IPv6 para on-premises"
#    #  destination = var.on_prem_cidr_block_ipv6
#    #  destination_type = "CIDR_BLOCK"
#    #}
#}
#
##resource "oci_core_route_table" "priv_route_table" {
##    #Required
##    compartment_id = local.compartment_id
##    vcn_id = oci_core_vcn.vcn.id
##
##    #Optional
##    display_name = "Tabela de rotas de rede privada"
##    freeform_tags = { "terraformed": "Please do not edit manually" }
##    route_rules {
##        #Required
##        network_entity_id = resource.oci_core_drg.terrario_drg.id
##
##        #Optional
##        cidr_block = var.route_table_route_rules_cidr_block
##        description = var.route_table_route_rules_description
##        destination = var.route_table_route_rules_destination
##        destination_type = var.route_table_route_rules_destination_type
##    }
##}
##resource "oci_core_drg_route_table_route_rule" "test_drg_route_table_route_rule" {
##    #Required
##    drg_route_table_id = oci_core_drg.terrario_drg.oci_core_drg_route_table.id
##    destination = var.drg_route_table_route_rule_route_rules_destination
##    destination_type = var.drg_route_table_route_rule_route_rules_destination_type
##    next_hop_drg_attachment_id = oci_core_drg_attachment.op_drg_attachment.id
##}
#
#
#
#
#
#### IPv6 BLOCK ###
##resource "oci_core_ipv6" "terrario_ipv6" {
##    #Required
##    vnic_id = oci_core_vnic_attachment.test_vnic_attachment.id
##
##    #Optional
##    display_name = var.ipv6_display_name
##    ip_address = var.ipv6_ip_address
##    ipv6subnet_cidr = var.ipv6_ipv6subnet_cidr
##}
#
#
#
#### Tunnel IPSEC ###
##resource "oci_core_ipsec" "test_ip_sec_connection" {
##    #Required
##    compartment_id = local.compartment_id
##    cpe_id = oci_core_cpe.test_cpe.id
##    drg_id = oci_core_drg.test_drg.id
##    static_routes = var.ip_sec_connection_static_routes
##
##    #Optional
##    cpe_local_identifier = var.ip_sec_connection_cpe_local_identifier
##    cpe_local_identifier_type = var.ip_sec_connection_cpe_local_identifier_type
##    defined_tags = {"Operations.CostCenter"= "42"}
##    display_name = var.ip_sec_connection_display_name
##    freeform_tags = { "terraformed": "Please do not edit manually" }
##}
#
#
#
#### PRIVATE AREA ###
#resource "oci_core_security_list" "private_sec_list" {
#  compartment_id = local.compartment_id
#  vcn_id = resource.oci_core_vcn.vcn.id
#  display_name = var.priv_security_list_display_name
#  ingress_security_rules {
#    protocol = "all"
#    source   = "10.0.0.0/16"
#    source_type = "CIDR_BLOCK"
#  }
#  egress_security_rules {
#    stateless = false
#    protocol    = "all"
#    destination = "0.0.0.0/0"
#    destination_type = "CIDR_BLOCK"
#  }
#}
#
#
#
#### PUBLIC AREA ###
#resource "oci_core_security_list" "public_sec_list" {
#  compartment_id = local.compartment_id
#  vcn_id = resource.oci_core_vcn.vcn.id
#  display_name = var.pub_security_list_display_name
#  ingress_security_rules {
#    protocol = "all"
#    source   = "0.0.0.0/0"
#    source_type = "CIDR_BLOCK"
#  }
#  egress_security_rules {
#    stateless = false
#    protocol    = "all"
#    destination = "0.0.0.0/0"
#    destination_type = "CIDR_BLOCK"
#  }
#}
#