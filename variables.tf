#Oracle cloud provider vars
variable "tenancy_ocid" {
  type    = string  
}

variable "user_ocid" {
  type = string    
}

variable "privkey_path" {
  type = string    
}

variable "pubkey_path" {
  type = string    
}

variable "fingerprint" {
  type = string    
}

variable "region" {
  type = map
  default = {
    "vinhedo" = "sa-vinhedo-1"
  }    
}


#Compartment vars
variable "compartment_name" {
  type    = string
  default = "terrario"
}

variable "compartment_desc" {
  type    = string
  default = "Compartment criado com script terraform 'terracle'."
}


### Virtual Cloud Network vars ###
variable "vcn_name" {
  type = string
}

variable "vcn_dns_label" {
  type = string
}

variable ip_cidr_blocks {
  type = string
  description = "The VCN IP CIDR blocks"
  default = "10.0.0.0/16"
}


### Public Network vars ###
variable public_cidr_block {
  type = string
  description = "The public subnet CIDR block"
  default = "10.0.2.0/24"
}

variable public_subnet_name {
  type = string
  description = "The public subnet name"
  default = "PUBLIC SUBNET"
}

variable pubnet_dns_label {
  type = string
  description = "The public subnet dns label"
  default = "PUBNET"
}

#variable pub_ip_type {
#    type = string
#    description = "Possible values: NONE, RESERVED or EPHEMERAL"
#    default = "EPHEMERAL"
#}

variable pub_security_list_display_name {
  type = string
  description = "Nome da tabela de regras de segurança da rede publica"
  default = "Terrario-pubfw"
}


### Private Network vars ###
variable priv_cidr_block {
  type = string
  description = "The private subnet CIDR block"
  default = "10.0.1.0/24"
}

variable privnet_dns_label {
  type = string
  description = "The private subnet dns label"
  default = "PRIVNET"
}


### GATEWAYS ###
variable drg_display_name {
  type = string
  description = "Dynamic Routing Gateway display name"
  default = "terraform_drg"
}

variable internet_gateway_enabled {
  type = bool
  description = "Deve conectar com a internet"
  default = true
}

variable internet_gateway_display_name {
  type = string
  description = "Nome do gateway internet"
  default = "Terrario-internet"
}

variable priv_security_list_display_name {
  type = string
  description = "Nome da tabela de regras de segurança da rede privada"
  default = "Terrario-privfw"
}

variable on_prem_cidr_block {
  type = string
  description = "On premises network's CIDR BLOCK"
  default = "192.168.0.0/24"
}



### CPE VARS ###
variable "cpe_shape" {
  type = map
  description = "Possible values: libreswan, other"
  default = {
    "libreswan" = 1,
    "other" = 6
  }
  #shape_id deveria ser: libreswan = "311edd80-34ce-440a-91fe-4fa139f404c6"
}
variable cpe_ip {
  type = string
  description = "IP do roteador local - ver https://www.yougetsignal.com/tools/open-ports/"
  default = "191.191.20.69"
}
variable cpe_name {
  type = string
  description = "Nome do CPE"
  default = "Homelab"
}



### Compute instance vars ###
variable "image_id" {
  type = string
  default = "vinhedo"
}

variable "shape" {
  type    = string
  default = "ARM"
}

variable "display_name" {
  type = string
}

variable "instance_count" {
  type = number
  default = 1
}

variable "state" {
  type = string
  description = "Possible values: RUNNING, STOPPED"
  default = "STOPPED"
}

variable "instance_desc" {
  type = list(object({
    name        = string
    state       = string
  }))
}

variable "assign_public_ip" {
  type = list
  description = "Ter, no mínimo, o mesmo número de elementos quanto forem indicados em instance_count"
  default = [ false, false, false, false ]
}

variable "ssh_pubkey_file" {
  type = string
  description = "Public key file to be placed into all VMs"
}
