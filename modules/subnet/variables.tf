variable compartment_id {
  type = string
  description = "Compartment to place this subnet into"
}

variable vcn_id {
  type = string
  description = "ID of the VCN this subnet is in"
}

variable cidr_block {
    type = string
    description = "This subnet's CIDR block"
}

#variable display_name {
#    type = string
#    description = "This subnet's name"
#    default = "UNNAMED TOFU-ED SUBNET"
#}

variable dns_label {
    type = string
    description = "This subnet's DNS label"
    default = "tofu_net"
}

variable block_internet_ingress {
    type = bool
    description = "Flag that blocks by default internet ingress into this subnet"
    default = true
}

variable route_table_id {
    type = string
    description = "Route table to be used in this network"
}
