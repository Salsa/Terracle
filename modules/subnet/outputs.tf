output "id" {
    description = "This subnet's id"
    value       = oci_core_subnet.subnet.id
}

output "ipv6_cidr_block" {
    description = "The configured CIDR block for this subnet"
    value       = oci_core_subnet.subnet.ipv6cidr_block
}

output "cidr_block" {
    description = "The configured CIDR blocks for this subnet"
    value       = oci_core_subnet.subnet.cidr_block
}

output "display_name" {
    description = "This subnet's diplayed name"
    value       = oci_core_subnet.subnet.display_name
}

output "dns_label" {
    description = "This subnet's label as shown on DNS"
    value       = oci_core_subnet.subnet.dns_label
}

output "router_ip" {
    description = "This subnet's router's IP"
    value       = oci_core_subnet.subnet.virtual_router_ip
}