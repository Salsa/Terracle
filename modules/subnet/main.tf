resource "oci_core_subnet" "subnet" {
  compartment_id  = var.compartment_id
  vcn_id          = var.vcn_id
  cidr_block      = var.cidr_block
 
  # Caution: For the route table id, use module.vcn.nat_route_id.
  # Do not use module.vcn.nat_gateway_id, because it is the OCID for the gateway and not the route table.
  route_table_id            = var.route_table_id
  #security_list_ids        = [oci_core_security_list.public_sec_list.id]
  display_name              = var.dns_label
  dns_label                 = var.dns_label
  #ipv6cidr_block           = var.ipv6_cidr_block
  prohibit_internet_ingress = var.block_internet_ingress
}

resource "oci_core_internet_gateway" "internet_gateway" {
  count = var.block_internet_ingress ? 0: 1

  #Required
  compartment_id = var.compartment_id
  vcn_id         = var.vcn_id
  
  #Optional - DONT SET ROUTE TABLE ID HERE. IT WILL FAIL
  enabled = true
  display_name   = "Internet-GW"
}

resource "oci_core_route_table" "internet_route" {
  count = var.block_internet_ingress ? 0: 1

  compartment_id  = var.compartment_id
  vcn_id          = var.vcn_id

#  route_rules {
#    #Required
#    network_entity_id = resource.oci_core_internet_gateway.internet_gateway[0].id
#    #Optional
#    destination_type = "CIDR_BLOCK"
#    destination      = "0.0.0.0/0"
#    description      = "Route to Internet"
#   }
}

resource "oci_core_default_route_table" "internet_route" {
  count = var.block_internet_ingress ? 0: 1

  manage_default_resource_id = var.route_table_id
  compartment_id  = var.compartment_id

  route_rules {
    #Required
    network_entity_id = resource.oci_core_internet_gateway.internet_gateway[0].id
    #Optional
    #destination_type = "CIDR_BLOCK"
    destination      = "0.0.0.0/0"
    description      = "Route to Internet"
   }
}
