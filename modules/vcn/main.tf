resource "oci_core_vcn" "vcn" {
  #Required
  compartment_id = var.compartment_id

  #Optional
  cidr_blocks   = var.ip_cidr_blocks
  display_name  = var.vcn_name
  dns_label     = var.vcn_name

  freeform_tags = { "tofu-ed": "Please do not edit manually" }
  is_ipv6enabled = true
  is_oracle_gua_allocation_enabled = true
}