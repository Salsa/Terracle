variable "compartment_id" {
  description = "Compartment this will be stored in"
  type        = string
}

variable "vcn_name" {
  description = "This VCN's name"
  type        = string
}

#variable "dns_label" {
#  description = "This VCN's dns label"
#  type        = string
#}

variable "ip_cidr_blocks" {
  description = "This VCN's IP CIDR blocks"
  type        = list(string)
  default     = ["10.0.0.0/16"]
}
