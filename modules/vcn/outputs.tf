output "id" {
    description = "This VCN's id"
    value       = oci_core_vcn.vcn.id
}

output "cidr_blocks" {
    description = "The configure CIDR blocks for this VCN"
    value       = oci_core_vcn.vcn.cidr_blocks
}

output "display_name" {
    description = "This VNC's diplayed name"
    value       = oci_core_vcn.vcn.display_name
}

output "dns_label" {
    description = "This VCN's label as shown on DNS"
    value       = oci_core_vcn.vcn.dns_label
}

output "default_route_table_id" {
    value = oci_core_vcn.vcn.default_route_table_id
} 
