data "oci_core_cpe_device_shapes" "oci_cpe_device_shapes" {
}

resource "oci_core_cpe" "conn_cpe" {
    #Required
    compartment_id  = var.compartment_id
    ip_address      = var.cpe_ip

    #Optional
    cpe_device_shape_id = data.oci_core_cpe_device_shapes.oci_cpe_device_shapes.cpe_device_shapes[6].cpe_device_shape_id
    display_name        = var.cpe_name
    freeform_tags       = { "TOFU-ED": "Please do not edit manually" }
    is_private          = true
}