output "id" {
  description = "This cpe's id"
  value       = oci_core_cpe.conn_cpe.id
}

output "shape_id" {
  description = "This cpe's shape id"
  value       = oci_core_cpe.conn_cpe.cpe_device_shape_id
}

output "ip" {
  description = "This cpe's ip"
  value       = oci_core_cpe.conn_cpe.ip_address
}

output "cpe_shapes" {
  description = "CPE shapes disponíveis"
  value       = data.oci_core_cpe_device_shapes.oci_cpe_device_shapes
}
