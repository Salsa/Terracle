variable compartment_id {
  type = string
  description = "The compartment where this cpe will reside"
}

variable cpe_name {
  type = string
  description = "This cpe's name"
}

variable cpe_ip {
  type = string
  description = "This cpe's IP"
}
