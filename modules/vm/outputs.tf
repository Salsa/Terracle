#output "id" {
#    description = "This host's id"
#    value       = oci_core_instance.hosts.id
#}
#
#output "cidr_blocks" {
#    description = "The configure CIDR blocks for this VCN"
#    value       = oci_core_instance.vcn.cidr_blocks
#}
#
#output "display_name" {
#    description = "This VNC's diplayed name"
#    value       = oci_core_instance.vcn.display_name
#}
#
#output "dns_label" {
#    description = "This VCN's label as shown on DNS"
#    value       = oci_core_instance.vcn.dns_label
#}

#Availability Domain
output "name-of-first-availability-domain" {
  value = data.oci_identity_availability_domains.ads.availability_domains[0].name
}
##FKtd:SA-VINHEDO-1-AD-1

output "all-availability-domains-in-your-tenancy" {
  value = data.oci_identity_availability_domains.ads.availability_domains
}

output "public_ip" {
  value = oci_core_instance.hosts[0].public_ip
}