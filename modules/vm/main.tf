### Identifica availability domain
data "oci_identity_availability_domains" "ads" {
  compartment_id = var.compartment_id
}

##Sempre utilizar apenas um availability domain
locals {
  availability_domain = data.oci_identity_availability_domains.ads.availability_domains[0].name
}

locals {
  hosts_desc = {
    for index in range (0, var.instance_count) :
    index => {
      name        = format("${var.hostname}-0%d", index + 1)
      ip_addr     = format("10.0.0.%d", index + 1)
      state       = var.states[index]
      use_pub_ip  = var.open[index]
      subnet_id   = var.subnet_ids[index]
    }
  }
}


resource "oci_core_instance" "hosts" {
  availability_domain = local.availability_domain
  compartment_id      = var.compartment_id
  shape               = var.shape_type[var.shape]
  
  for_each = local.hosts_desc
  shape_config {
    memory_in_gbs = 24/var.instance_count
    ocpus         = 4/var.instance_count
  }
  source_details {
    source_id   = var.image_id[var.image]
    source_type = "image"
  }

  state        = each.value["state"]
  display_name = each.value["name"]

  create_vnic_details {
    subnet_id         = each.value["subnet_id"]
    display_name      = each.value["name"]
    assign_public_ip  = each.value["use_pub_ip"]
  }

  metadata = {
    ssh_authorized_keys = file(var.ssh_pubkey_file)
  }
}

##module "compute-instance" {
##  source = "oracle-terraform-modules/compute-instance/oci"
##  version = "2.4.1"
##
##  compartment_ocid  = local.compartment_id
##  source_ocid       = var.image_id["vinhedo"]
##  subnet_ocids      = [resource.oci_core_subnet.main_subnet.id]
##
##  #instance_count             = var.instance_count
##  #ad_number                  = 1 # AD number to provision instances. If null, instances are provisionned in a rolling manner starting with AD1  
##  #instance_display_name      = var.instance_display_name
##  #public_ip                  = var.pub_ip_type
##  #ssh_public_keys            = file(var.pubkey_path)
##  #instance_state             = var.state
##  #boot_volume_backup_policy  = "disabled"  
##}
#