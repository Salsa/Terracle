variable "compartment_id" {
  description = "Compartment this will be stored in"
  type        = string
}

variable "shape" {
  type    = string
  default = "ARM"
}

variable "shape_type" {
    type = map
    default = {
        "ARM" = "VM.Standard.A1.Flex"
        "AMD" = "VM.Standard.E2.1.Micro"
    }
}

variable "instance_count" {
  type = number
  default = 1
  description = "How many VMs to create"
}

variable "states" {
  type        = list
  default     = ["STOPPED", "STOPPED", "STOPPED", "STOPPED"]
  description = "VM's state after creation"
}

variable "open" {
  type        = list
  default     = ["false", "false", "false", "false"]
  description = "VM's usage of public IPs"
}

variable "subnet_ids" {
  type        = list
  description = "VMs' subnet"
}

variable "images" {
  type    = list
  default = ["vinhedo", "vinhedo", "vinhedo", "vinhedo"]
}

variable "image" {
  type    = string
  default = "vinhedo"
}

variable "image_id" {
  type = map
  default = {
    "vinhedo" = "ocid1.image.oc1.sa-vinhedo-1.aaaaaaaaqcx7nffbqwxftsa7bkt6v45b35rkgtrodctaqqwi3p4fiyjcoolq"
  }
}

variable "hostname" {
  type    = string
  default = "vhost"
}

variable "ssh_pubkey_file" {
  type = string
  description = "Public key file to be placed into all VMs"
}