### SETUP TERRAFORM CLOUD ###
terraform {
  cloud {
    organization  = "Salsa-lab"
    hostname      = "app.terraform.io"

    workspaces {
      name = "Homelab-OCI"
    }
  }
}

