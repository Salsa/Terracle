### Compartment ###
output "compartment_name" {
  value = oci_identity_compartment.compartment.name
}

output "compartment_ocid" {
  value = oci_identity_compartment.compartment.id
}



### CPE ###
#output "cpe_shapes" {
#  value = module.router.cpe_shapes
#}


### Virtual Cloud Network ###
#output "vcn_id" {
#  description = "OCID da VCN criada"
#  value = resource.oci_core_vcn.vcn.id
#}
#output "main_route_table_id" {
#  description = "OCID of the internet-route table. This route table has an internet gateway to be used for public subnets"
#  value = resource.oci_core_internet_gateway.world_gw.id
#}
#
#output "vcn" {
#  description = "Full VCN description"
#  value = resource.oci_core_vcn.vcn
#}
#


### Availability Domain ###
output "name-of-first-availability-domain" {
  value = module.hosts.name-of-first-availability-domain
}
###FKtd:SA-VINHEDO-1-AD-1

output "all-availability-domains-in-your-tenancy" {
  value = module.hosts.all-availability-domains-in-your-tenancy
}

output "public_ip" {
  value = module.hosts.public_ip
}

##output "public-ip-for-compute-instance" {
##  value = oci_core_instance.ubuntu-host.public_ip
##}
#
##output "public-ips" {
##  value = module.instances.public_ip
##}
